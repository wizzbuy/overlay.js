(function (window, document, $) {

  // Constructor.
  function Overlay ($el, options) {
    this.options = $.extend({}, Overlay.defaults, options);

    this.$el = $el;

    this
      .prepare()  // Prepare DOM
      .bind()     // Bind events
      .insert()   // Insert into DOM
      .position() // Position overlay content
      .show();    // Show on screen
  }

  Overlay.stack = [];

  Overlay.defaults = {

    // Either `from-above` or `from-within`. Set to `false` to disable.
    animation: 'from-above',

    // Either `dark` or `light`. Set to `false` to disable.
    backdrop: 'dark',

    // Where to insert the overlay container.
    target: 'body'

  };

  Overlay.prototype.visibile = false;

  /*
   * DOM preparation.
   * ----------------
   */

  Overlay.prototype.prepare = function () {
    /*
     * DOM Layout:
     *
     * div.overlay-Container
     *   div.overlay-Wrapper
     *     div.overlay-Body
     */
    this.container  = this.make_container();
    this.wrapper    = this.make_wrapper();
    this.body       = this.make_body();

    this.wrapper.appendTo(this.container);
    this.body.appendTo(this.wrapper);

    // Append container to target if it's not yet parented.
    if (this.container.parent().length === 0) {
      $(this.options.target).prepend(this.container);
    }

    return this;
  };

  Overlay.prototype.insert = function () {
    if (this.$el.parent().length) {
      this.body.append(this.$el.clone());
    }
    else {
      this.body.append(this.$el);
    }

    this.trigger('ready', this);

    Overlay.stack.push(this);

    return this;
  };

  Overlay.prototype.make_container = function () {
    var container = $('.overlay-Container');

    if (container.length === 0) {
      container = $('<div class="overlay-Container"></div>');
    }

    return container;
  };

  Overlay.prototype.make_wrapper = function () {
    var classes = [
      'overlay-Wrapper',
      'overlay--hidden'
    ];

    if (this.options.backdrop) {
      classes.push('overlay--backdrop-' + this.options.backdrop);
    }

    if (this.options.animation) {
      classes.push('overlay--animation-' + this.options.animation);
    }

    return $('<div class="' + classes.join(' ') + '"></div>');
  };

  // Prepare and return the box container (where content is appended).
  Overlay.prototype.make_body = function () {
    return $('<div class="overlay-Body"><div>');
  };

  /*
   * Events handling methods.
   * ------------------------
   */

  Overlay.prototype.bind = function () {
    var self = this;

    // Listen for clicks on the background to close the overlay.
    setTimeout(function () {
      self.wrapper.on('click', function (e) {
        if (self.wrapper.is(e.target)) {
          self.close('backdrop');
        }
      });
    }, 125);

    // Listen for clicks buttons to close the overlay.
    this.body.on('click', 'button.close', function () {
      self.close('button');
    });

    // Listen for resize events on the DOM.
    this.body.on('resize', function () {
      self.position();
    });

    return this;
  };

  Overlay.prototype.on = function () {
    this.$el.on.apply(this.$el, arguments);
    return this;
  };

  Overlay.prototype.once = function () {
    this.$el.one.apply(this.$el, arguments);
    return this;
  };

  Overlay.prototype.trigger = function () {
    this.$el.trigger.apply(this.$el, arguments);
    return this;
  };

  /*
   * Body viewport handling.
   * -----------------------
   */

  Overlay.prototype.should_unlock = function () {
    var visibles = Overlay.stack.filter(function (overlay) {
      return overlay.visible;
    });

    return visibles.length === 0;
  };

  Overlay.prototype.lock = function () {
    var $body = $('body');

    if ($body.css('overflow') !== 'hidden') {
      var width = $body.width();

      $body.css('overflow', 'hidden');
      $body.css('padding-right', $body.width() - width);
    }

    return this;
  };

  Overlay.prototype.unlock = function () {
    var $body = $('body');

    $body.css('overflow', 'auto');
    $body.css('padding-right', 0);

    return this;
  };

  /*
   * Visibility handling.
   * --------------------
   */

  Overlay.prototype.show = function (callback) {
    this.lock();

    this.wrapper.addClass('overlay--shown');
    this.wrapper.removeClass('overlay--hidden');

    this.visible = true;

    this.trigger('shown', this);

    if (typeof callback === 'function') {
      callback('shown');
    }

    return this;
  };

  Overlay.prototype.hide = function (callback) {
    if (this.should_unlock()) {
      this.unlock();
    }

    this.wrapper.addClass('overlay--hidden');
    this.wrapper.removeClass('overlay--shown');

    this.visible = false;

    this.trigger('hidden', this);

    if (typeof callback === 'function') {
      callback('hidden');
    }

    return this;
  };

  Overlay.prototype.close = function (origin) {
    var self = this;

    this.trigger('closed', origin);

    this.hide(function () {
      self.wrapper.remove();

      Overlay.stack.pop();

      if (self.should_unlock()) {
        self.unlock();
      }
    });

    return this;
  };

  Overlay.prototype.position = function () {
    var width = this.body.innerWidth();

    this.body.css({
      marginLeft: -width / 2,
    });

    return this;
  };

  // Register as a jQuery plugin.
  $.fn.overlay = function () {
    var first = Array.prototype.shift.call(arguments);
    var rest = Array.prototype.slice.call(arguments);

    var length;

    /*
     * TODO Use Array-like primitives (pop, shift, etc)..
     */

    if (first === 'close-all') {
      length = Overlay.stack.length;

      while (Overlay.stack.length > 0) {
        Overlay.stack[0].close('api');
      }

      return length;
    }
    else if (first === 'close-first') {
      if (Overlay.stack.length === 0) {
        return 0;
      }

      Overlay.stack[0].close('api');

      return 1;
    }
    else if (first === 'close-last') {
      length = Overlay.stack.length;

      if (length === 0) {
        return 0;
      }

      Overlay.stack[length - 1].close('api');

      return 1;
    }

    return $(this).each(function () {
      var $el = $(this);

      // Attempt to retrieve stored Overlay instance on DOM element.
      var overlay = $el.data('overlay');

      if (typeof first === 'string') {
        if (!overlay) {
          return; // Nothing to do.
        }

        // Call the actual method.
        overlay[first].call(overlay, rest);
      }
      else {
        if (overlay) {
          return; // Nothing to do.
        }

        overlay = new Overlay($el, first)
          .on('closed', function () {
            $el.removeData('overlay');
          });

        // Store Overlay instance on DOM element.
        $el.data('overlay', overlay);
      }
    });
  };

  // Give access to the stack.
  $.fn.overlay.stack = Overlay.stack;

  // Give access to the defaults.
  $.fn.overlay.defaults = Overlay.defaults;

  // Register event handlers to support DOM attributes based triggering.
  $(document).on('click.overlay.api', '[data-toggle=overlay]', function () {
    var $el = $(this);

    $($el.attr('href') || $el.data('href'))
      .overlay($el.data());
  });

})(window, document, jQuery);
