# Overlay

Stackable overlay engine for jQuery (bring your own design).

## Features

- Stackable
- Fully evented
- Bring your own design
- Close buttons support
- Optional backdrop
- Animation (customizable)
- Versatile


## Usage

```html
<div class="welcome-notice">
  <h1>Welcome!</h1>
  <p>Yada yada.</p>
  <p>
    <button class="close">
      Okay.
    </button>
  </p>
</div>
```

```javascript
$('.welcome-notice').overlay({
    animation: 'from-within',
    backdrop: 'light'
});
```


## Options

The following options are supported:

- `animation`: `from-above`, `from-within` or *false* to disable
- `backdrop`: `dark`, `light` or *false* to disable
- `target`: Where to insert the overlay container; defaults to `body`.


## Advanced usage

- `$.fn.overlay.stack`
- `$.fn.overlay.defaults`
- `$.fn.overlay('close-all')`
- `$.fn.overlay('close-first')`
- `$.fn.overlay('close-last')`


## License

ISC © 2014 Acute Analytics / Benoit Myard <bmyard@acute.fr>
